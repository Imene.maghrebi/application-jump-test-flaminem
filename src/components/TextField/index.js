import React, { useState } from 'react';
import { TextField as MTextField } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import PropTypes from 'prop-types';

const TextField = formProps => {
  const [showPassword, setShowPassword] = useState(false);

  const {
    input,
    label,
    type,
    meta: { touched, error },
    isPassword = false,
    className = '',
    ...custom
  } = formProps;
  return (
    <div className="text-field-box">
      <MTextField
        className={`${className} text-field`}
        error={!!(touched && error)}
        helperText={touched && error ? error : ''}
        label={label}
        type={type || (isPassword && !showPassword ? 'password' : 'text')}
        {...input}
        {...custom}
      />
      {isPassword && (
        <div className="password-eye">
          <InputAdornment position="end">
            <IconButton
              aria-label="Toggle password visibility"
              onClick={() => {
                setShowPassword(!showPassword);
              }}
            >
              {showPassword ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </InputAdornment>
        </div>
      )}
    </div>
  );
};

TextField.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  isPassword: PropTypes.bool,
  className: PropTypes.string,
  meta: PropTypes.object,
};

export default TextField;
