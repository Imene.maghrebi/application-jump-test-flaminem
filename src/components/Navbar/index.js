import React from 'react';
import { AppBar } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

function Navbar() {
  return (
    <AppBar position="fixed" className="navbar">
      <Typography variant="h6" className="title">
        Application JUMP
      </Typography>
    </AppBar>
  );
}

export default Navbar;
