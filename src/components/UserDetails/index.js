import React from 'react';
import PropTypes from 'prop-types';

const UserDetails = ({ selectedUser }) => {
  return (
    <>
      <div className="details-title">Détails</div>
      <div className="details-content">
        <div> Adresse:</div>
        <div>
          <div>{selectedUser.address.street}</div>
          <div>{selectedUser.address.city}</div>
        </div>
      </div>
    </>
  );
};

UserDetails.propTypes = {
  selectedUser: PropTypes.object,
};

export default UserDetails;
