import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Login, Users } from 'pages';
import { Navbar } from 'components';
import PropTypes from 'prop-types';
function App(props) {
  const {
    user,
    location: { pathname },
  } = props;
  return (
    <div>
      {pathname === '/' && user ? (
        <Redirect to="/users" />
      ) : (
        <Redirect to="/login" />
      )}
      <Navbar />
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/users" component={Users} />
      </Switch>
    </div>
  );
}

const mapStateToProps = ({ user: { user } }) => ({
  user,
});

App.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
};

export default connect(mapStateToProps)(App);
