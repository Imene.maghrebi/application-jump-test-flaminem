import React from 'react';
import { connect } from 'react-redux';
import { LoginUserAction } from 'actions';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { TextField } from 'components';
import PropTypes from 'prop-types';
import validate from './validate';

function Login(props) {
  const { handleSubmit, history } = props;

  const handleForm = formValues => {
    props.LoginUserAction(formValues, history);
  };
  return (
    <div className="login ">
      <div className="login-title">
        <h4>Veuillez vous authentifier</h4>
      </div>

      <form onSubmit={handleSubmit(handleForm)} className="form login-form">
        <svg>
          <rect />
        </svg>
        <Field
          name="email"
          isPassword={false}
          component={TextField}
          label="Utilisateur"
        />
        <Field
          className="form-password"
          name="password"
          isPassword
          component={TextField}
          label="Mot de passe"
        />
        <div className="button-box">
          <button type="submit">Login</button>
        </div>
      </form>
    </div>
  );
}

Login.propTypes = {
  handleSubmit: PropTypes.func,
  history: PropTypes.object,
  LoginUserAction: PropTypes.func,
};

export default reduxForm({
  form: 'loginForm',
  validate,
})(
  connect(
    null,
    { LoginUserAction },
  )(withRouter(Login)),
);
