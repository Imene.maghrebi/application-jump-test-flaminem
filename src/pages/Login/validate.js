export default formValues => {
  const { email, password } = formValues;
  const errors = {};
  const requiredFields = ['email', 'password'];

  requiredFields.forEach(field => {
    if (!formValues[field]) {
      errors[field] = `Required ${field}`;
    }
  });

  if (email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
    errors.email = 'adresse mail invalide';
  }

  if (password) {
    if (password.length < 6)
      errors.password = 'le mot de passe doit contenir au moins 6 caractères';
  }

  return errors;
};
