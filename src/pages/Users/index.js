import React, { Component } from 'react';
import { UserDetails } from 'components';
import Radio from '@material-ui/core/Radio';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { data } from '../../constants';

class Users extends Component {
  state = {
    selectedRow: null,
  };

  toggleRow(original) {
    const { selectedRow } = this.state;
    this.setState({
      selectedRow: selectedRow === original ? null : original,
    });
  }

  columns = [
    {
      Header: '',
      id: 'radio',
      accessor: '',
      Cell: ({ original }) => (
        <Radio
          checked={this.state.selectedRow === original}
          onClick={() => this.toggleRow(original)}
        />
      ),

      sortable: false,
      width: 45,
    },

    {
      Header: 'Nom',
      accessor: 'lastName',
      sortable: true,
    },
    {
      Header: 'Prénom',
      accessor: 'firstName',
      sortable: true,
    },
  ];

  render() {
    return (
      <div className="users">
        <h1 className="title">Liste des clients</h1>

        <div>
          <ReactTable data={data} columns={this.columns} defaultPageSize={5} />
          {this.state.selectedRow && (
            <div className="details">
              <UserDetails selectedUser={this.state.selectedRow} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Users;
