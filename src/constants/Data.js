export const data = [
  {
    id: 1,
    firstName: 'Leanne ',
    lastName: 'Graham',
    address: {
      street: '14, rue Pergolèse',
      city: '75116 Paris',
    },
  },
  {
    id: 2,
    firstName: 'Ervin  ',
    lastName: 'Howell',
    address: {
      street: '15, rue Pergolèse ',
      city: '75117 Lyon',
    },
  },
  {
    id: 3,
    firstName: 'Clementine ',
    lastName: 'Bauch',
    address: {
      street: '16, rue Pergolèse ',
      city: '75118 Toulouse',
    },
  },
  {
    id: 4,
    firstName: 'Patricia ',
    lastName: 'Lebsack',
    address: {
      street: '17, rue Pergolèse ',
      city: '75119 Strasbourg',
    },
  },
  {
    id: 5,
    firstName: 'Chelsey ',
    lastName: 'Dietrich',
    address: {
      street: '18, rue Pergolèse ',
      city: '75120 Grenoble',
    },
  },
  {
    id: 6,
    firstName: 'Dennis ',
    lastName: 'Schulist',
    address: {
      street: '19, rue Pergolèse ',
      city: '75121 Bordeaux',
    },
  },
  {
    id: 7,
    firstName: 'Kurtis ',
    lastName: 'Weissnat',
    address: {
      street: '20, rue Pergolèse ',
      city: '75122 Marseille',
    },
  },
  {
    id: 8,
    firstName: 'Nicholas ',
    lastName: 'Runolfsdottir',
    address: {
      street: '21, rue Pergolèse ',
      city: '75123 Nantes',
    },
  },
  {
    id: 9,
    firstName: 'Glenna ',
    lastName: 'Reichert',
    address: {
      street: '22, rue Pergolèse ',
      city: '75124 Rennes',
    },
  },
  {
    id: 10,
    firstName: 'Clementina ',
    lastName: 'DuBuque',
    address: {
      street: '23, rue Pergolèse ',
      city: '75125 Lille',
    },
  },
];
