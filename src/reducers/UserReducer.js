import { LOGIN_USER_SUCCESS } from '../constants';

const initialState = {};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        user: payload,
      };

    default:
      return state;
  }
};
