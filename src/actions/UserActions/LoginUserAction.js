import { NotificationManager } from 'react-notifications';

import { LOGIN_USER_SUCCESS } from '../../constants';

export default ({ email, password }, history) => dispatch => {
  if (email === 'test@example.com' && password === '123456') {
    const user = { email, password };
    dispatch({ type: LOGIN_USER_SUCCESS, payload: user });

    NotificationManager.success('Connexion avec succès');

    history.push('/users');
  } else {
    NotificationManager.error(
      ' identifiant erroné. Email test: test@example.com  mot de passe: 123456',
    );
  }
};
